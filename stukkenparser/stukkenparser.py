#! /usr/bin/env python

from copy import deepcopy
from functools import partial, wraps
from pathlib import Path
from typing import Any, Dict, Iterator, Optional, Tuple, Union
from zipfile import ZipFile

import lxml.html  # typing: ignore
import regex as re  # typing: ignore
from lxml import etree  # typing: ignore

import sys
##verwijs directory naar waar jij de parser scripts hebt staan dit moet dus aangepast worden!
sys.path.insert(0, 'C:/Users/Administrator/stukkenparser-main/stukkenparser')

from onderdelen import (
    Alinea,
    Box,
    DefLijst,
    Figuur,
    Kop,
    Lijst,
    Margetekst,
    Onderdeel,
    Opsomming,
    Stuk,
    Tabel,
    Tussenkop,
)


def log_tokens(func):
    @wraps(func)
    def logger(*args):
        for tok in func(*args):
            print(f"{tok.elt.tag} {tok.elt.attrib} -> {tok.token}: {tok.text}")
            yield tok

    return logger


class Lexer:
    """The Lexer reads an html file and generates a token stream.

    Attributes:
        parser: lxml.html.HTMLParser
        tree: etree._Element
    """

    def __init__(self, source: Union[str, bytes], config: Dict[str, Any]) -> None:
        """Initialize the lexer.

        Args:
            source: html to be parsed
            config: dictionary with configuration directives
        """

        self.parser: etree._FeedParser = lxml.html.HTMLParser(
            remove_comments=True, remove_pis=True
        )

        if not re.search(r"<.*?>", source):
            raise ValueError("Source is not an HTML document.")
        try:
            self.tree: etree._Element = etree.fromstring(source, self.parser)
        except ValueError as e:
            raise ValueError(f"found {type(source)}") from e

        self.config = config

    @classmethod
    def from_file(cls, source: Path, config: Dict[str, Any]) -> "Lexer":
        """Initialize Lexer from a file.

        Args:
            source: path to file
            config: dictionary with configuration directives
        """
        lexer = cls.__new__(cls)
        lexer.parser = lxml.html.HTMLParser(remove_comments=True, remove_pis=True)
        lexer.config = config
        lexer.tree = etree.fromstring(cls._read_source(source), lexer.parser)
        lexer.settings = config.get("lexer", {})
        lexer.entrypoint = lexer.settings.get("entrypoint", None)

        return lexer

    @staticmethod
    def _read_source(source: Path) -> bytes:
        if not source.exists():
            raise FileNotFoundError(f"{source}")
        if source.suffix == ".zip":
            with ZipFile(source) as zf:
                for f in zf.namelist():
                    if f.endswith("html"):
                        return zf.read(f)
        else:
            with source.open("rb") as hf:
                return hf.read()
        return None

    @staticmethod
    def class_contains(elt, cls):
        """Helper method for checking css classes."""
        return cls in elt.get("class", "")

    def _head_tokens(self) -> Iterator[Onderdeel]:
        return NotImplemented

    def _body_tokens(self) -> Iterator[Onderdeel]:
        return NotImplemented

    def _stream(self) -> Iterator[Onderdeel]:
        yield from self._head_tokens()
        yield from self._body_tokens()

    def __iter__(self) -> Iterator[Onderdeel]:
        return self

    def __next__(self) -> Optional[Union[Onderdeel, Kop]]:
        if not hasattr(self, "_iter"):
            self._iter = self._stream()
        return next(self._iter)


class OBLexer(Lexer):
    """Lexer for officielebekendmakingen.nl."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.entrypoint = "div[@id='broodtekst']"

    def _head_tokens(self) -> Iterator[Onderdeel]:
        """Yields h1 elements that occur before the document's main text."""
        headers = self.tree.xpath(f"//{self.entrypoint}/h1")
        for head in headers:
            yield Kop(head)

    def _body_tokens(self) -> Iterator[Onderdeel]:
        """Tokenizes the document into Onderdelen."""
        divs = self.tree.xpath(f"//{self.entrypoint}/div")
        for div in divs:
            for elem in div:
                if re.match(r"h\d", elem.tag):
                    if "note-close" in elem.get("class"):
                        if "href" in elem.attrib:  # b/c it's a foot note
                            continue
                    yield Kop(elem, self.config)
                elif elem.tag == "p":
                    yield Alinea(elem)
                elif "voet noot" in elem.get(
                    "class", ""
                ):  # divs with foot notes at the bottom of the page
                    continue
                elif elem.tag == "div":
                    yield from self._process_div(elem)

    def _process_div(self, elem: lxml.html.HtmlElement) -> Iterator[Onderdeel]:
        for elt in elem.iterchildren():
            elt_class = elt.get("class", "")
            if elt.tag == "div":
                if "plaatje" in elt_class:
                    yield Figuur(elt)
                elif "alineagroep" in elt_class:
                    yield Opsomming(elt)
                elif "margetekst" in elt_class:
                    yield Margetekst(elt, self.config)
                elif elt_class in ("artikel", "wijziging"):
                    yield from self._process_div(elt)
                elif "box" in elt_class:
                    yield Box(elt, self.config)
            elif elt.tag == "ul":
                yield Lijst(elt)
            elif elt.tag == "dl":
                yield DefLijst(elt)
            elif elt.tag == "table":
                yield Tabel(elt, self.config)
            elif elt.tag == "p":
                if "tussenkop" in elt_class:
                    yield Tussenkop(elt, self.config)
                else:
                    yield Alinea(elt)
            elif re.match(r"h\d", elt.tag):
                if "note-close" in elt.get("class"):
                    if "href" in elt.attrib:
                        continue
                yield Kop(elt, self.config)
            else:
                continue


class WettenLexer(Lexer):
    """Lexer for wetten.nl."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.entrypoint = "div[@class='wetgeving']/div"

    def _head_tokens(self) -> None:
        yield from []

    def _body_tokens(self) -> Iterator[Onderdeel]:
        """Tokenizes the document into rdelen."""
        divs = self.tree.xpath(f"//{self.entrypoint}")
        for div in divs:
            for elem in div:
                if re.match(r"h\d", elem.tag):
                    yield Kop(elem, self.config)
                elif elem.tag == "p":
                    yield Alinea(elem)
                elif elem.tag == "div":
                    yield from self._process_div(elem)

    def _process_div(self, elem: lxml.html.HtmlElement) -> Iterator[Onderdeel]:
        for elt in elem.iterchildren():
            # class_contains = partial(self.class_contains, elt)
            if elt.tag == "div":
                yield from self._process_div(elt)
            elif elt.tag == "ul":
                if self.class_contains(elt.find("li"), "action--"):
                    continue
                yield Lijst(elt)
            elif elt.tag == "dl":
                yield DefLijst(elt)
            elif elt.tag == "table":
                yield Tabel(elt, self.config)
            elif elt.tag == "p":
                if elt.find("strong") is not None:
                    yield Tussenkop(elt, self.config)
                else:
                    yield Alinea(elt)
            elif re.match(r"h\d", elt.tag):
                yield Kop(elt, self.config)
            else:
                continue


class Parser:
    """The Parser imposes structure on the token stream generated by the lexer.

    Entry point of this class is the `parse()` method. E.g.::

        parser = stukkenparser.Parser()
        stuk = parser.parse(fn)

    Attributes:
        stream: the token stream
        cur: current token
        nxt: next token (lookahead)
        glob_config: optional default configuration (see :ref:`config-ref`)
    """

    def __init__(self, config: Dict[str, Any] = {}, lexer: Optional[Lexer] = None):
        """Initialize the parser.

        Args:
            config: dict with parsing instructions
            lexer: an instance of `stukkenparser.Lexer`. Default: OBlexer.
        """
        self.glob_config: Dict[str, Any] = config

        if lexer:
            self.lexer = lexer
        else:
            self.lexer = OBLexer

        self.cur: Optional[Union[Onderdeel, Kop, Tussenkop]] = None
        self.nxt: Optional[Union[Onderdeel, Kop, Tussenkop]] = None

    def _setup(
        self, source: Union[str, bytes], config: Dict[str, Any], from_file: bool = False
    ) -> None:
        if config:
            cfg = self._update_config(deepcopy(self.glob_config), config)
        else:
            cfg = self.glob_config

        if from_file:
            self.stream = self.lexer.from_file(source, cfg)
        else:
            self.stream = self.lexer(source, cfg)

        self.cur = None
        self.nxt = None
        self.consume()

    def _update_config(
        self, source: Dict[str, Any], overrides: Dict[str, Any]
    ) -> Dict[str, Any]:
        for key, value in overrides.items():
            if isinstance(value, dict) and value:
                returned = self._update_config(source.get(key, {}), value)
                source[key] = returned
            else:
                source[key] = overrides[key]
        return source

    def parse(
        self,
        source: Union[str, Path],
        local_config: Dict[str, Any] = {},
        from_file: bool = False,
    ) -> Stuk:
        """Parse an HTML document.

        Args:
            source: HTML document or path to file.
            local_config: dict containing specific parsing instructions (optional).
            from_file: must be True when passing a path

        Returns:
            `stukkenparser.Stuk`
        """
        if isinstance(source, Path):
            self._setup(source, local_config, from_file=True)
        else:
            self._setup(source, local_config, from_file)

        stuk = Stuk(self.stream.tree.find("./head"), self.stream.config)

        while self.nxt is not None:
            if self.is_sectie():
                stuk.add(self.sectie())
            elif self.is_paragraaf():
                stuk.add(self.paragraaf())
            elif self.is_content():
                stuk.add(self.content())
            else:
                raise ValueError(f"Invalid token: {self.nxt}")
        # return Stuk(root)
        return stuk

    def consume(self) -> None:
        """Move to the next token."""
        self.cur = self.nxt
        self.nxt = next(self.stream, None)

    def _token_p(self, t: Tuple[Any, ...]) -> bool:
        if self.nxt is not None:
            return isinstance(self.nxt, t)
        return False

    def is_sectie(self) -> bool:
        return self._token_p((Kop,)) and not self.is_paragraaf()

    def is_paragraaf(self) -> bool:
        return self._token_p((Tussenkop,))

    def is_content(self) -> bool:
        return self._token_p((Alinea, Lijst, Opsomming, Tabel, Figuur))

    def is_tabel(self) -> bool:
        return self._token_p((Tabel,))

    def sectie(self) -> Kop:
        """Process tokens of type `Kop`.

        When a `Kop` token is encountered, all tokens following it are added as its children, until
        the parser encounters a `Kop` of a lower or equal `level`.
        """
        self.consume()
        kop: Kop = self.cur  # type: ignore
        while self.nxt is not None:
            if self.is_sectie():
                if self.nxt.level <= kop.level:  # type: ignore
                    return kop
                else:
                    kop.add(self.sectie())
            elif self.is_paragraaf():
                kop.add(self.paragraaf())
            else:
                kop.add(self.content())
        return kop

    def paragraaf(self) -> Tussenkop:
        """Process tokens of type `Tussenkop` and `Margetekst`.

        When a `Tussenkop` token is encountered, all tokens following it are added as its children, until
        the parser encounters a `Kop`, or a `Tussenkop` or `Margetekst` of a lower or equal `level`.
        """
        self.consume()
        par: Tussenkop = self.cur  # type: ignore
        while self.nxt is not None:
            if self.is_paragraaf():
                # ignore type checker: type is checked in self.is_paragraaf().
                # NOTE: maybe that's to implicit?
                if self.nxt.level <= par.level:  # type: ignore
                    return par
                else:
                    par.add(self.paragraaf())
            elif self.is_sectie():
                return par
            else:
                par.add(self.content())
        return par

    def content(self) -> Onderdeel:
        if self.nxt is not None:
            self.consume()
        return self.cur
