import sys

##verwijs directory naar waar jij de parser scripts hebt staan dit moet dus aangepast worden!
sys.path.insert(0, 'C:/Users/Administrator/stukkenparser-main/stukkenparser')

import stukkenparser

import adapters

from pathlib import Path

##voeg de globale configuratie die geld voor jou stuk toe (per soort stuk is er een aparte global_config)
global_config = {
  "embedding": {
      "tussenkop": {
          "opmaak": {
              "tussenkop_vet": "2",
              "tussenkop_cur": "3",
              "tussenkop_ondlijn": "4",
              "tussenkop_rom": "5"
          }
      }
  },
  "types": {
      "kop": {
          "(?i)\\d\\.? beleidsartikel(en)?": "beleidsartikelen",
          "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
      },
      "tabel": {
          "(?i)budgettaire gevolgen": "b_tabel"
      }
  },
}

##maak de parser
parser = stukkenparser.Parser(global_config) 

##voeg een lokale configuratie toe die specifiek is voor dat hoofdstuk
local_config ={
    "embedding": {
        "tussenkop": {
            "opmaak": {
                "tussenkop_rom": "5",
                "tussenkop_ondlijn": "3",
                "tussenkop_cur": "3.5"
            },
            "titel": {
                "Motivering": 2.5,
                "Producten": 2.5,
                "14.01.04 Investeringsruimte": 2
            }
        }
    },
    "types": {
        "kop": {
            "(?i)\\d\\.\\d productartikel(en)?": "beleidsartikelen"
        },
        "tabel": {
            "(?i)budgettaire gevolgen": "b-tabel"
        }
    }
}

##Parse het stuk, verwijs naar het pad waar jij het stuk hebt staan, geef de lokale config mee, en voeg from_file=True toe
stuk = parser.parse(Path(r"C:\\Users\\Administrator\\JV\\kst-35470-A-1.html"), local_config, from_file=True)

##Mogelijkheden wat je met dit stuk kunt

##itereren
for o in stuk.onderdelen():
    print(o.text)


##zoeken naar lege onderdelen
lege_onderdelen = stuk.find(lambda o: hasattr(o, "children") and len(o) == 0)

##artikelen uit de stukken halen
artikelen = stuk.find_type("artikel")

##Zoeken naar een keyword
keywords = stuk.find_string("infrastructuur", case=False)

##Het stuk wegschrijven naar XML
xml_adapter = adapters.XMLAdapter(stuk)

xml = xml_adapter.to_string()

with open("xml_filetest.xml", "w", encoding="utf-8") as xmlf:
    xmlf.write(xml)


##Dit is hoe de RDF adapter in theorie zou moeten werken maar de RDF adapter zelf moet nog aangepast worden.
#rdf_adapter = adapters.RDFAdapter(stuk)

#rdf = rdf_adapter.to_string()

#with open("rdf_file.ttl", "w", encoding = "utf-8") as rdff:
#    rdff.write(rdf)
